﻿using ACE.Server.Entity;
using ACE.Server.Network;
using ACE.Server.Network.Enum;
using ACE.Server.Network.Managers;
using ACE.Server.Network.Packets;
using log4net;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ACE.Mods.WebSockets {
    public class WebSocketSession : Session, IDisposable {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly HttpContext context;

        public WebSocket WebSocket { get; }
        public ushort ClientId { get; }
        public ushort ServerId { get; }

        public WebSocketSession(WebSocket webSocket, HttpContext context, IPEndPoint endPoint, ushort clientId, ushort serverId) : base(null, endPoint, clientId, serverId) {
            WebSocket = webSocket;
            this.context = context;
            ClientId = clientId;
            ServerId = serverId;
        }

        public void Listen() {
            Task.Run(async () => {
                while (WebSocket.State == WebSocketState.Open || WebSocket.State == WebSocketState.Connecting) {
                    var buffer = new byte[1024 * 4];
                    var result = await WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                    var packet = new ClientPacket();

                    if (packet.Unpack(buffer, result.Count)) {
                        log.Info($"Unpack packet: {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort} // {packet.Header}");
                        
                    }
                }
            });
        }

        public void Dispose() {
            
        }

        internal void SendLoginRequestReject(CharacterError error) {
            // First we must send the connect request response
            var connectRequest = new PacketOutboundConnectRequest(
                Timers.PortalYearTicks,
                Network.ConnectionData.ConnectionCookie,
                Network.ClientId,
                Network.ConnectionData.ServerSeed,
                Network.ConnectionData.ClientSeed);
            Network.ConnectionData.DiscardSeeds();
            Network.EnqueueSend(connectRequest);

            // Then we send the error
            SendCharacterError(error);

            Network.Update();
        }
    }
}
