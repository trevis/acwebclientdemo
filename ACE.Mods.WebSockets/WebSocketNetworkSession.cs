﻿using ACE.Server.Network;
using ACE.Server.Network.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE.Mods.WebSockets {
    public class WebSocketNetworkSession : NetworkSession {
        public WebSocketSession Session { get; }

        public WebSocketNetworkSession(WebSocketSession session, ushort clientId, ushort serverId) : base(session, null, clientId, serverId) {
            Session = session;
        }

    }
}
