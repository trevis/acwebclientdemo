﻿using ACE.Database.Models.Shard;
using ACE.Entity.Enum;
using ACE.Server.Command;
using ACE.Server.Mods;
using ACE.Server.Network;
using ACE.Server.Network.Enum;
using HarmonyLib;
using log4net;
using Microsoft.AspNetCore.Connections;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ACE.Mods.WebSockets {
    [HarmonyPatch]
    public class PatchClass {
        [HarmonyPrefix]
        [HarmonyPatch(typeof(Session), MethodType.Constructor, new Type[] { typeof(ConnectionListener), typeof(IPEndPoint), typeof(ushort), typeof(ushort) })]
        public static bool PreCtorSession(ConnectionListener connectionListener, IPEndPoint endPoint, ushort clientId, ushort serverId, ref Session __instance) {
            if (connectionListener == null) {
                var field = typeof(Session).GetField("<EndPoint>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
                field.SetValue(__instance, endPoint);
                __instance.Network = new WebSocketNetworkSession(__instance as WebSocketSession, clientId, serverId);

                var field2 = typeof(Session).GetField("<Characters>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
                field2.SetValue(__instance, new List<Character>());
                return false;
            }

            return true;
        }

        [HarmonyPrefix]
        [HarmonyPatch(typeof(NetworkSession), "SendPacketRaw", new Type[] { typeof(ServerPacket) })]
        public static bool PreSendPacketRaw(ServerPacket packet, ref NetworkSession __instance) {
            if (__instance is WebSocketNetworkSession webSocketNetworkSession) {
                byte[] buffer = ArrayPool<byte>.Shared.Rent((int)(PacketHeader.HeaderSize + (packet.Data?.Length ?? 0) + (packet.Fragments.Count * PacketFragment.MaxFragementSize)));

                try {
                    packet.CreateReadyToSendPacket(buffer, out var size);

                    try {
                        webSocketNetworkSession.Session.WebSocket.SendAsync(buffer, System.Net.WebSockets.WebSocketMessageType.Binary, true, CancellationToken.None);
                    }
                    catch (SocketException ex) {
                        // Unhandled Exception: System.Net.Sockets.SocketException: A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram into was smaller than the datagram itself
                        // at System.Net.Sockets.Socket.UpdateStatusAfterSocketErrorAndThrowException(SocketError error, String callerName)
                        // at System.Net.Sockets.Socket.SendTo(Byte[] buffer, Int32 offset, Int32 size, SocketFlags socketFlags, EndPoint remoteEP)
                        Console.WriteLine(ex.ToString());
                        webSocketNetworkSession.Session.Terminate(SessionTerminationReason.SendToSocketException, null, null, ex.Message);
                    }
                }
                finally {
                    ArrayPool<byte>.Shared.Return(buffer, true);
                }
                return false;
            }
            //Return false to override
            //return false;

            //Return true to execute original
            return true;
        }
    }
}
