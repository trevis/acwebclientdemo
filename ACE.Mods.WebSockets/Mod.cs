﻿using ACE.Entity.Enum;
using ACE.Server.Command;
using ACE.Server.Mods;
using ACE.Server.Network;
using HarmonyLib;

namespace ACE.Mods.WebSockets {
    public class Mod : IHarmonyMod {
        //If Harmony is set to debug it creates a log on Desktop
        public const bool DEBUGGING = true;
        //Point to your mod directory
        public static string ModPath = Path.Combine(ModManager.ModPath, "ACE.Mods.WebSockets");

        //IDs are used by Harmony to separate multiple patches
        const string ID = "com.ACE.ACEmulator.ACE.Mods.WebSockets";
        protected static Harmony Harmony { get; set; } = new(ID);

        private bool disposedValue;

        #region Initialize / Dispose (called by ACE)
        public void Initialize() {
            //Patch everything in the mod with Harmony attributes
            Harmony.PatchAll();

            WebSocketManager.Start();
        }

        //https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    WebSocketManager.Stop();

                    Harmony.UnpatchAll(ID);
                }
                disposedValue = true;
            }
        }

        public void Dispose() {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}