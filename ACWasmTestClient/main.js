// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

import { dotnet } from './dotnet.js'

const { setModuleImports, getAssemblyExports, getConfig } = await dotnet
    .withDiagnosticTracing(false)
    .withApplicationArgumentsFromQuery()
    .create();

const serverAddress = "ws://localhost:3000";
let didLogin = false;

const log = (message) => {
    console.log(message);
    const preNode = document.createElement("pre");
    preNode.appendChild(document.createTextNode(`${message}\n`));
    document.getElementById("out").appendChild(preNode);
    window.scrollTo(0, document.body.scrollHeight);
};

setModuleImports('main.js', {
    log,
    ui: {
        showCharacterSelect: (characters) => {
            characters.forEach((character) => {
                const node = document.createElement("a");
                node.href = '#'; 
                node.appendChild(document.createTextNode(`>> Login as ${character}`));
                document.getElementById("out").appendChild(node);
                window.scrollTo(0, document.body.scrollHeight);
                node.addEventListener("click", (event) => {
                    event.preventDefault();
                    if (!didLogin) {
                        exports.MyClass.LoginCharacter(character);
                        didLogin = true;
                    }
                })
            })
        }
    },
    socket: {
        send: (port, data) => {
            socket.send(data);
        }
    }
});

const config = getConfig();
const exports = await getAssemblyExports(config.mainAssemblyName);
let socket = undefined;

document.getElementById("connect").addEventListener("click", () => {
    didLogin = false;
    log(`Connecting to server: ${serverAddress}`)

    // Create WebSocket connection.
    socket = new WebSocket(serverAddress);

    // Connection opened
    socket.addEventListener("open", (event) => {
        exports.MyClass.Start(document.getElementById("user").value, document.getElementById("pass").value);
    });

    // Listen for messages 
    socket.addEventListener("message", (event) => {
        var fileReader = new FileReader();
        fileReader.onload = function (evt) {
            var ar = new Uint8Array(evt.target.result);
            exports.MyClass.Receive(ar);
        };

        fileReader.readAsArrayBuffer(event.data);
    });
})

const tick = () => {
    exports.MyClass.Update();
    setTimeout(tick, 100);
}

setTimeout(tick, 100);

const chatInput = document.getElementById("chat");
chatInput.addEventListener("keyup", ({ key }) => {
    if (key === "Enter") {
        const message = chatInput.value;
        chatInput.value = "";
        exports.MyClass.Say(message);
    }
})

document.getElementById("connect").removeAttribute("disabled");
await dotnet.run();