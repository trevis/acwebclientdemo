﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestClient.Net.Packets {
	class SendEnterWorldFragment : Fragment {
		public int Id;
		public string AccountName;

		public SendEnterWorldFragment(int id, string accountName) : base(0xf657, 4) {
			Id = id;
			AccountName = accountName;
		}

		protected override void OnSerialize(BinaryWriter writer) {
			base.OnSerialize(writer);
			writer.Write(Id);
			PacketWriter.WriteString16(writer, AccountName);
		}
	}
}
