﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestClient.Lib.Constants;
using TestClient.Net.Packets;

namespace TestClient.Lib.Net.Packets.Events {
    class TalkEvent : EventFragment {
        public string Text;

        public TalkEvent(string text) : base(0x0015) {
            Text = text;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            PacketWriter.WriteString16(writer, Text);
        }
    }
}
