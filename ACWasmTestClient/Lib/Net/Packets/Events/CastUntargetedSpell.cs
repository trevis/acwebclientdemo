﻿
    using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestClient.Lib.Constants;
using TestClient.Net.Packets;

namespace TestClient.Lib.Net.Packets.Events {
    class CastUntargetedSpell : EventFragment {
        public int SpellId;

        public CastUntargetedSpell(int spellId) : base(0x0048) {
            SpellId = spellId;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            writer.Write(SpellId);
        }
    }
}
