﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestClient.Lib.Constants;
using TestClient.Net.Packets;

namespace TestClient.Lib.Net.Packets.Events {
    class ChangeCombatModeEvent : EventFragment {
        public CombatState CombatState;

        public ChangeCombatModeEvent(CombatState combatState) : base(0x0053) {
            CombatState = combatState;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            writer.Write((uint)CombatState);
        }
    }
}
