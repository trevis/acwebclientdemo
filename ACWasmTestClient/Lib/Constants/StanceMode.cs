﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestClient.Lib.Constants {
    public enum StanceMode : ushort {
        HandCombat = 0x003C,
        NonCombat = 0x003D,
        SwordCombat = 0x003E,
        BowCombat = 0x003F,
        SwordShieldCombat = 0x0040,
        CrossbowCombat = 0x0041,
        UnusedCombat = 0x0042,
        SlingCombat = 0x0043,
        TwoHandedSwordCombat = 0x0044,
        TwoHandedStaffCombat = 0x0045,
        DualWieldCombat = 0x0046,
        ThrownWeaponCombat = 0x0047,
        SpellCombat = 0x0049,
        BowNoAmmo = 0x00E8,
        CrossBowNoAmmo = 0x00E9,
        AtlatlCombat = 0x0138,
        ThrownShieldCombat = 0x0139,
    }
}
