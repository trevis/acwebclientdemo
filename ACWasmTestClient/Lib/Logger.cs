﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestClient.Lib {
    class Logger {
        public static void LogException(Exception ex) {
            try {
                Console.WriteLine("============================================================================");
                Console.WriteLine(DateTime.Now.ToString());
                Console.WriteLine("Error: " + ex.Message);
                Console.WriteLine("Source: " + ex.Source);
                Console.WriteLine("Stack: " + ex.StackTrace);
                if (ex.InnerException != null) {
                    Console.WriteLine("Inner: " + ex.InnerException.Message);
                    Console.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                }
                Console.WriteLine("============================================================================");
                Console.WriteLine("");
            }
            catch {
            }
        }
    }
}
