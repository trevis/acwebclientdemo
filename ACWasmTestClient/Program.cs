using System;
using System.Reflection.Metadata;
using System.Runtime.InteropServices.JavaScript;
using System.Threading;
using TestClient.Bot;
using TestClient.Net.Messages;
using TestClient.Net;
using System.Collections.Generic;

Console.WriteLine("Hello, Browser!");

public partial class MyClass {
    private static NetworkManager networkManager;
    private static Bot bot;

    [JSExport]
    internal static void Start(string username, string password) {
        bot = null;
        networkManager?.Dispose();
        networkManager = null;

        MyClass.Log("Starting");

        MessageParser.Initialize("messages.xml");

        networkManager = new NetworkManager();
        bot = new Bot(networkManager, username, password);
    }

    [JSExport]
    internal static void Update() {
        if (bot != null) {
            bot.net.DoNetworking();
            bot.CallHandlers();
        }
    }

    [JSExport]
    internal static void Receive(byte[] buffer) {
        if (bot != null) {
            bot.net.OnReceive(buffer);
        }
    }

    [JSExport]
    internal static void LoginCharacter(string characterName) {
        if (bot != null) {
            bot.LoginCharacter(characterName);
        }
    }

    [JSExport]
    internal static void Say(string message) {
        if (bot != null) {
            bot.Chat.Say(message);
        }
    }

    [JSImport("log", "main.js")]
    internal static partial void Log(string message);


    [JSImport("socket.send", "main.js")]
    internal static partial void Send(int port, byte[] data);

    [JSImport("ui.showCharacterSelect", "main.js")]
    internal static partial void ShowCharacterSelect(string[] accountCharacters);
}
