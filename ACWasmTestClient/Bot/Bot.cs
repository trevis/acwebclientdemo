﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestClient.Lib;
using TestClient.Net;
using TestClient.Net.Messages;
using TestClient.Net.Packets;

namespace TestClient.Bot {
	enum ServerType {
		GDLE,
		ACE
	}

	partial class Bot {
		public string AccountName = "";
		public string Password = "";
		public ServerType ServerType = ServerType.ACE;
		public uint CharacterId = 0;
		public string Host = "127.0.0.1";
		public int Port = 9000;

        public World.WorldManager World;
        public World.ChatManager Chat;
        public World.Character Character;

        public delegate void MessageHandler(Message message);

        Dictionary<string, uint> AccountCharacters = new Dictionary<string, uint>();
        private Dictionary<uint, List<MessageHandler>> messageHandlers = new Dictionary<uint, List<MessageHandler>>();
        ConcurrentQueue<Message> messageQueue = new ConcurrentQueue<Message>();
        public NetworkManager net;

		public Bot(NetworkManager networkManager, string username, string password) {
			AccountName = username;
			Password = password;
			net = networkManager;

            net.MessageReceived += Net_MessageReceived;

            World = new World.WorldManager(this);
            Chat = new World.ChatManager(this);

            switch (ServerType) {
				case ServerType.GDLE:
					net.Connect(Host, Port, $"{AccountName}:{Password}", "");
					break;

				case ServerType.ACE:
					net.Connect(Host, Port, AccountName, Password);
					break;
			}
		}

        public void AddHandler(uint messageType, MessageHandler handler) {
            if (!messageHandlers.ContainsKey(messageType)) {
                messageHandlers[messageType] = new List<MessageHandler>();
            }

            messageHandlers[messageType].Add(handler);
        }

        public void RemoveHandler(uint messageType, MessageHandler handler) {
            if (!messageHandlers.ContainsKey(messageType)) {
                messageHandlers[messageType] = new List<MessageHandler>();
            }

            messageHandlers[messageType].Remove(handler);
        }

        public void CallHandlers() {
            Message message;
            while (messageQueue.TryDequeue(out message)) {
                if (messageHandlers.ContainsKey(message.Type)) {
                    var handlers = messageHandlers[message.Type].ToArray();
                    foreach (var handler in handlers) {
                        //try {
                            if (handler != null) {
                                handler(message);
                            }
                        //}
                        //catch (Exception ex) { Logger.LogException(ex); }
                    }
                }
            }
        }

        private void Net_MessageReceived(object sender, MessageEventArgs e) {
            messageQueue.Enqueue(e.Message);

            MyClass.Log($"<<{e.Message.Type.ToString("X4")}");

			switch (e.Message.Type) {
				case 0xF658: //LoginCharacterSet
					AccountName = Convert.ToString(e.Message["zonename"]);
					int characterSlots = Convert.ToInt32(e.Message["characterCount"]);
					MessageStruct characters = e.Message.Struct("characters");

					AccountCharacters.Clear();

					for (int i = 0; i < characterSlots; i++) {
						string name = characters.Struct(i).Value<string>("name");
						int id = characters.Struct(i).Value<int>("character");
						AccountCharacters.Add(name, (uint)id);
					}
					break;

				case 0xF7E5: // start ddd interrogation
					Thread.Sleep(200);
					net.SendMessage(new DatabaseSyncFragment());
					net.SendMessage(new Fragment(0xF7C8, 4)); // Request Enter Game
					break;

				case 0xF7EA: // end ddd interrogation
					net.SendMessage(new Fragment(0xF7EA, 4)); // DDD End
					break;

				case 0xf7df: // EnterGameServerReady
                    MyClass.Log("------------ EnterGameServerReady ------------");

					MyClass.ShowCharacterSelect(AccountCharacters.Keys.ToArray());
					break;

				case 0xF7E1:
					string server = e.Message.Value<string>("server");
					int players = e.Message.Value<int>("players");
					int maxPlayers = e.Message.Value<int>("maxPlayers");

					if (maxPlayers <= 0) {
                        MyClass.Log($"Welcome to {server}. There are {players} players connected.");
					}
					else {
                        MyClass.Log($"Welcome to {server}. There are {players}/{maxPlayers} players connected.");
					}

					break;

				case 0xF7DC: // account booted
					string reason = e.Message.Value<string>("reason");
                    MyClass.Log($"Booted: {reason}");
					break;

				case 0xF659: // character error
                             //todo
                             // we were unable to login, probably because a character is already ingame
                             // try again in 5 seconds
                    MyClass.Log($"Character error...");
					Thread.Sleep(5000);
					net.SendMessage(new Fragment(0xF7C8, 4)); // Request Enter Game

					break;

			}
		}

		public void LoginCharacter(string characterName) {
			if (!AccountCharacters.TryGetValue(characterName, out var characterId)) {
				MyClass.Log($"Unable to find Character: {characterName}");
				return;
			}

			CharacterId = characterId;
            Character = new World.Character(this, (int)CharacterId);

            if (CharacterId != 0) {
                MyClass.Log($"Attempting to log in {LoginCharacter} ({CharacterId.ToString("X8")})");
                net.SendMessage(new SendEnterWorldFragment((int)CharacterId, AccountName));
                Thread.Sleep(200);
                // appear immediately
                net.SendEventMessage(new LoginCompleteNotificationEventFragment());
            }
        }
	}
}
