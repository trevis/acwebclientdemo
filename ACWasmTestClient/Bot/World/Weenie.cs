﻿using System;
using System.Collections.Generic;
using System.Text;
using TestClient.Net.Messages;

namespace TestClient.Bot.World {
    class Weenie {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int Type { get; private set; }
        public int Icon { get; private set; }
        public int Category { get; private set; }
        public int Behavior { get; private set; }

        public Weenie() {
        
        }

        new public string ToString() {
            return $"{Name} [{Id.ToString("X8")}]";
        }

        internal void UpdateFromMessage(Message message) {
            throw new NotImplementedException();
        }

        internal static Weenie FromMessage(Message message) {
            try {
                var weenie = new Weenie();
                var gameData = message.Struct("game");

                weenie.Id = message.Value<int>("object");
                weenie.Name = gameData.Value<string>("name");
                weenie.Type = gameData.Value<int>("type");
                weenie.Icon = gameData.Value<int>("icon");
                weenie.Category = gameData.Value<int>("category");
                weenie.Behavior = gameData.Value<int>("behavior");

                return weenie;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            return null;
        }
    }
}
