﻿using System;
using System.Collections.Generic;
using System.Text;
using TestClient.Lib;
using TestClient.Lib.Constants;
using TestClient.Lib.Net.Packets;
using TestClient.Lib.Net.Packets.Events;
using TestClient.Net.Messages;
using TestClient.Net.Packets;

namespace TestClient.Bot.World {
    class ChatManager {
        Bot bot;
        public ChatManager(Bot bot) {
            this.bot = bot;

            bot.AddHandler(0x02BB, OnHearSpeach);
            
            // this one has a range?
            bot.AddHandler(0x02BC, OnHearSpeach);

            bot.AddHandler(0x02BD, OnTell);

            // i think this might be broken?
            // not sure how or why...
            bot.AddHandler(0xF7B0, OnTell);
        }

        public void Say(string message) {
            bot.net.SendEventMessage(new TalkEvent(message));
        }

        private void OnHearSpeach(Message message) {
            PrintChatMessage(message);
        }

        private void OnTell(Message message) {
            var text = message.Value<string>("text");

            if (string.IsNullOrEmpty(text)) return;

            if (text.Contains("#peace")) {
                bot.Character.SetCombatState(CombatState.NonCombat);
            }
            if (text.Contains("#magic")) {
                bot.Character.SetCombatState(CombatState.Magic);
            }

            if (text.Contains("#combatstate")) {
                Say("CombatState: " + bot.Character.CombatState);
            }

            if (text.Contains("#position")) {
                Say("Position: " + bot.Character.Position.ToString());
                Say("Rotation: " + bot.Character.Rotation.ToString());
            }

            if (text.Contains("#primary")) {
                bot.Character.CastSpell(157); // summon primary portal I
            }

            if (text.Contains("#secondary")) {
                bot.Character.CastSpell(2648); // summon secondary portal I
            }

            if (text.Contains("#face")) {
                bot.Character.CastSpell(2648); // summon secondary portal I
            }

            PrintChatMessage(message);
        }

        private void PrintChatMessage(Message message) {
            try {
                var text = message.Value<string>("text");
                var senderName = message.Value<string>("senderName");
                var senderId = message.Value<int>("sender");
                ChatMessageType type = (ChatMessageType)message.Value<int>("type");

                MyClass.Log($"[{type}] {senderName}[{senderId.ToString("X8")}]: {text}");
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
    }
}
