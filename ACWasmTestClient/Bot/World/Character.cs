﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using TestClient.Lib.Constants;
using TestClient.Lib.Net.Packets;
using TestClient.Lib.Net.Packets.Events;
using TestClient.Net.Messages;
using TestClient.Net.Packets;

namespace TestClient.Bot.World {
    class Character {
        Bot bot;

        public int Id { get; private set; }

        public int Landcell;
        public Vector3 Position;
        public Quaternion Rotation;

        public CombatState CombatState { get; private set; }

        public Character(Bot bot, int id) {
            this.bot = bot;
            Id = id;
            CombatState = CombatState.NonCombat;
            Position = new Vector3();
            Rotation = new Quaternion();

            bot.AddHandler(0x02CD, OnPrivateUpdateInt);
            bot.AddHandler(0xF748, OnPositionEvent);
            bot.AddHandler(0xF745, OnCreateObject);
        }

        public void CastSpell(int spellId) {
            bot.net.SendEventMessage(new CastUntargetedSpell(spellId));
        }

        private void OnCreateObject(Message message) {
            var id = message.Value<int>("object");

            if (id != Id) return;

            bot.RemoveHandler(0xF745, OnCreateObject);

            var physics = message.Struct("physics");
            var flags = physics.Value<int>("flags");
            
            if ((flags & 0x00008000) != 0) { // Position data
                var position = physics.Struct("position");

                Landcell = position.Value<int>("landcell");
                Position.X = position.Value<float>("x");
                Position.Y = position.Value<float>("y");
                Position.Z = position.Value<float>("z");
                Rotation.W = position.Value<float>("wQuat");
                Rotation.X = position.Value<float>("xQuat");
                Rotation.Y = position.Value<float>("yQuat");
                Rotation.Z = position.Value<float>("zQuat");
            }
        }

        private void OnPrivateUpdateInt(Message message) {
            switch (message.Value<int>("key")) {
                case 0x00000028: // combat mode
                    CombatState = (CombatState)(message.Value<int>("value"));
                    break;
            }
        }

        private void OnPositionEvent(Message message) {
            var id = message.Value<int>("object");

            if (id != Id) return;

            Landcell = message.Struct("Position").Value<int>("landcell");

            Position.X = message.Struct("Position").Value<float>("x");
            Position.Y = message.Struct("Position").Value<float>("y");
            Position.Z = message.Struct("Position").Value<float>("z");
        }

        public void SetCombatState(CombatState combatState) {
            CombatState = combatState;
            bot.net.SendMessage(new ChangeCombatModeEvent(combatState));
        }
    }
}
