﻿using System;
using System.Collections.Generic;
using System.Text;
using TestClient.Lib;
using TestClient.Net;
using TestClient.Net.Messages;

namespace TestClient.Bot.World {
    class WorldManager {
        Bot bot;
        Dictionary<int, Weenie> weenies;

        public WorldManager(Bot bot) {
            this.bot = bot;
            weenies = new Dictionary<int, Weenie>();

            bot.AddHandler(0xF745, OnCreateObject);
        }

        private void OnCreateObject(Message message) {
            try {
                var id = message.Value<int>("object");

                if (weenies.ContainsKey(id)) {
                    weenies[id].UpdateFromMessage(message);
                }
                else {
                    var weenie = Weenie.FromMessage(message);

                    if (weenie != null) {
                        weenies.Add(id, weenie);

                        MyClass.Log($"OnCreateObject: {weenie.ToString()}");
                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
    }
}
